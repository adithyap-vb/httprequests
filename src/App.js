import { useEffect, useState } from "react";
import "./App.css";
const axios = require("axios");
function App() {
  const [posts, setPosts] = useState([]);
  let userId = 0;
  let title = "";
  let body = "";
  let url = "https://jsonplaceholder.typicode.com/posts";
  useEffect(() => {
    axios.get("https://jsonplaceholder.typicode.com/posts").then((response) => {
      setPosts(response.data);
      console.log(response.data);
      console.log("posts are " + posts);
    });
  }, []);

  useEffect(() => {
    console.log(posts);
  }, [posts]);

  const submitPost = (e) => {
    e.preventDefault();
    userId = document.getElementById("userId").value;
    title = document.getElementById("title").value;
    body = document.getElementById("body").value;

    const postObj = {
      id: userId,
      title,
      body,
    };

    try {
      axios.post(url, postObj).then((response) => {
        console.log(response);
      });
    } catch (error) {
      console.log("error is " + error);
    }
  };
  return (
    <>
      <h1>Posts</h1>
      <form>
        <label for="userId">USER ID</label>
        <br />
        <input type="number" id="userId" />
        <br />
        <label for="title">Title</label>
        <br />
        <input type="text" id="title" />
        <br />
        <label for="body">Body</label>
        <br />
        <input type="text" id="body" />
        <br />
        <button
          type="submit"
          onClick={(e) => {
            submitPost(e);
          }}
        >
          Submit
        </button>
      </form>
    </>
  );
}

export default App;
